# Domibus 4.2.6

This repo is made for containerizing Domibus to make it more portable. It is currently made for our narrow use case but
if you want to extend with further configuration or functionality contributions are welcome.

## Prerequisites

This project _ONLY_ contains the Domibus installation and assumes the database is already up and running.
For testing purposes the following docker image can be used:

```sh
docker run --name=domibus-sql \
  -e MYSQL_DATABASE=domibus \
	-e MYSQL_RANDOM_ROOT_PASSWORD=true \
	-e MYSQL_USER=edelivery \
	-e MYSQL_PASSWORD=edelivery \
	-p 8006:3306 \
	-d mysql/mysql-server:latest
```

## Usage

### Seed database

To start the Domibus instance you need to seed the database. Currently the scripts for Domibus 4.2.6 are copied in to the `scripts` folder
in this repo. However if you manually want to update the version later on the scripts are included in the Domibus release file.

To seed our test database run:

```
mysql -h 127.0.0.1 -P 8006 -pedelivery -uedelivery domibus < ./scripts/mysql-4.2.6.ddl
mysql -h 127.0.0.1 -P 8006 -pedelivery -uedelivery domibus < ./scripts/mysql-4.2.6-data.ddl
```

### Configure Domibus settings

Modify `scripts/domibus.properties` to fit your needs. Common fields to change are:

```ini
domibus.database.serverName=127.0.0.1
domibus.database.port=8006

domibus.datasource.xa.property.user=edelivery
domibus.datasource.xa.property.password=edelivery

domibus.datasource.user=edelivery
domibus.datasource.password=edelivery


domibus.security.keystore.location=/path/to/./secrets/gateway_keystore.jks
domibus.security.keystore.password=my-secret-keystore-password
domibus.security.key.private.alias=domibus-keystore-alias
domibus.security.key.private.password=my-secret-keystore-password

domibus.security.truststore.location=/path/to/gateway_truststore.jks
domibus.security.truststore.password=my-secret-truststore-password
```

### (OPTIONAL) Modify File Plugin configuration

If you are going to use the File Plugin you might want to edit the `scripts/fs-plugin-properties` file.
Note that if you want to change the path where files are stored you also need to make sure that the directory exists in the container.

### (OPTIONAL) Modify env variables

If you want to change env variables used by Domibus you can modify `scripts/setenv.sh`.

Example:

```sh
export JAVA_OPTS="$JAVA_OPTS -Xms128m -Xmx20148m "
export JAVA_OPTS="$JAVA_OPTS -Ddomibus.config.location=$CATALINA_HOME/conf/domibus"
```

### (OPTIONAL) Create self-signed certificates

Followed [this guide](https://stackoverflow.com/questions/47434877/how-to-generate-keystore-and-truststore) for generating the key and truststore

1. Generate keystore:

```
keytool -genkey -alias domibus-keystore-alias -keyalg RSA -keystore ./secrets/gateway_keystore.jks -keysize 2048
```

2. Generate new ca-cert and ca-key:

```
openssl req -new -x509 -keyout ca-key -out ca-cert
```

3. Extract cert/creating cert sign req(csr):

```
keytool -keystore ./secrets/gateway_keystore.jks -alias domibus-keystore-alias -certreq -file cert-file
```

4. Sign the “cert-file” and cert-signed wil be the new cert:

```
openssl x509 -req -CA ca-cert -CAkey ca-key -in cert-file -out cert-signed -days 365 -CAcreateserial -passin pass:my-secret-keystore-password
```

5. Import the ca-cert to keystore file:

```
keytool -keystore ./secrets/gateway_keystore.jks -alias CARoot -import -file ca-cert
```

6. Import cert-signed to keystore:

```
keytool -keystore ./secrets/gateway_keystore.jks -alias domibus-keystore-alias -import -file cert-signed
```

8. Generate truststore:

```
keytool -keystore ./secrets/gateway_truststore.jks -alias domibus-truststore-alias -import -file ca-cert
```

### (OPTIONAL) Add additional CA certificates

If you want to add additional CA certificates to the image during the build process you can copy them in to the `certs` folder before the build.

### Build the image

```
docker build -t domibus .
```

### Run container

To run the container you need to attach your modified properties file as well as the truststore and the keystore.
Run the command below and after a little while the Domibus instance is available at http://localhost:8080/domibus with the
default username `admin` and default password `123456`

```
docker run --rm -d \
	-v $(pwd)/scripts/domibus.properties:/app/cef_edelivery/conf/domibus/domibus.properties \
	-v $(pwd)/scripts/fs-plugin.properties:/app/cef_edelivery/conf/domibus/plugins/config/fs-plugin.properties \
	-v $(pwd)/secrets:/secrets \
	-p 8080:8080/tcp \
	domibus:latest
```
