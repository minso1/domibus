# FROM ubuntu:20.04 
FROM sapmachine:11.0.9 
WORKDIR /app
# Install Java etc
RUN apt-get update && apt install wget unzip -y

# Download and unzip domibus 5.0.1
RUN wget -q -O domibus.zip https://ec.europa.eu/digital-building-blocks/artifact/content/repositories/public/eu/domibus/domibus-distribution/5.0.1/domibus-distribution-5.0.1-tomcat-full.zip
RUN unzip -q domibus.zip -d /app/domibus_root
RUN mv /app/domibus_root/domibus /app/cef_edelivery
RUN rm -rf /app/domibus_root && rm domibus.zip

# Download and unzip mysql connector
RUN wget -q -O mysql.zip https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-8.0.26.zip
RUN unzip -q mysql.zip -d /app/mysql
RUN cp /app/mysql/mysql-connector-java-8.0.26/mysql-connector-java-8.0.26.jar /app/cef_edelivery/lib
RUN rm -rf /app/mysql && rm mysql.zip

# Download and unzip File System Plugin
RUN wget -q -O fsplugin.zip https://ec.europa.eu/digital-building-blocks/artifact/content/repositories/public/eu/domibus/domibus-distribution/5.0.1/domibus-distribution-5.0.1-default-fs-plugin.zip
RUN unzip -q fsplugin.zip -d /app/fs
RUN mkdir /app/cef_edelivery/plugins && mkdir /app/cef_edelivery/plugins/conf
RUN cp -r /app/fs/conf/domibus/plugins/lib/* /app/cef_edelivery/conf/domibus/plugins/lib
RUN ls /app/cef_edelivery/conf/domibus/plugins/lib
RUN rm -rf /app/fs && rm fsplugin.zip

# Create directory for file plugin
RUN mkdir data && mkdir /app/data/MAIN

COPY scripts/setenv.sh /app/cef_edelivery/bin/setenv.sh
RUN chmod +x /app/cef_edelivery/bin/*.sh
RUN chmod -R a+rw /app

# Add certificates:
COPY certs/*.crt /usr/local/share/ca-certificates/
RUN update-ca-certificates

# TODO: Generalize this
RUN keytool -noprompt -trustcacerts -importcert -file /usr/local/share/ca-certificates/LV_ROOT_CA.crt -alias lvroot -keystore /usr/lib/jvm/sapmachine-11/lib/security/cacerts -storepass changeit
RUN keytool -noprompt -trustcacerts -importcert -file /usr/local/share/ca-certificates/LV_SUB_CA.crt -alias lvsub -keystore /usr/lib/jvm/sapmachine-11/lib/security/cacerts -storepass changeit
RUN keytool -noprompt -trustcacerts -importcert -file /usr/local/share/ca-certificates/LV_TST_SUB_CA.crt -alias lvtst -keystore /usr/lib/jvm/sapmachine-11/lib/security/cacerts -storepass changeit
# Note: Here you can inject your `domibus.properties` file if you want it integrated in the image. 
# However, since it contains credentials it should probably be mounted at runtime

EXPOSE 8080 8443
CMD /app/cef_edelivery/bin/catalina.sh run
